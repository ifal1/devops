Neste exemplo, almeja-se a disponibilização de uma aplicação básica HTML em um servidor NGINX.

* Construção da imagem:

docker build . -t my_nginx

* Execução do container:

docker run --name meu_container -d -p 8080:80 my_nginx
